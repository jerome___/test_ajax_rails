###
Why reference error about "$' is not referenced ?

I just followed the rails-5.2 guide about rails and javascript...
what's wrong there ? what should i miss in the guide ?

Answer: yes, rails-5.2 argument is to said that "rails doesn't need jQuery"
So in fact, rails-ujs is supposed (like lot of other stuff) remplaced jQuery
But in fact, most of the time, users need jQuery 
(and rails-ujs seems to be poor documented)

SOLUTION:
1/ remove: require rails-ujs in application.js
2/ add gem 'jquery-rails" inside Gemfile
3/ add require jquery3 and require jquery_ujs inside applications.js (on top)
4/ enjoy the life now ...

OTHER SOLUTION:
1/ install jquery from yarn (yarn add jquery)
2/ add require jquery (top of the list application.js, before rails-ujs)
3/ then you can try to use jquery and rails-ujs together... (good luck)
###

$(document).ready ->
  $("#new_article").on "ajax:success", (event) ->
    $("#update_view").click()
  $("#new_article").on "ajax:error", (event) ->
    $("#ajax").fadeIn(100).html("AJAX FAILURE").fadeOut(2000)

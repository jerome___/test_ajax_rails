$("#ajax").fadeIn(100).html("<p>article <%= @article.title %> updated</p>").fadeOut(2000)
$("#update_view").click()
$("#form_title").html "New"
$("#article_form").html "<%= j render 'form', article: @article %>"
$("#article_title").attr("value", "")
$("#article_content").val("")
